package main

import (
	"context"
	"fmt"
	blogpb "gRPC-CRUD/proto"
	"io"
	"log"

	"google.golang.org/grpc"
)

func main() {
	// Create gRPC channel
	opts := grpc.WithInsecure()
	conn, err := grpc.Dial("localhost:50051", opts)
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer conn.Close()

	// Create stub
	client := blogpb.NewBlogServiceClient(conn)

	// RPC Calls
	// Create Blog
	fmt.Println("Creating the blog")
	blog := &blogpb.Blog{
		AuthorId: "Irving",
		Title:    "Go Client",
		Content:  "Content sent from Go Client",
	}
	createBlogRes, err := client.CreateBlog(context.Background(), &blogpb.CreateBlogReq{Blog: blog})
	if err != nil {
		log.Fatalf("Unexpected error: %v", err)
	}
	fmt.Printf("Blog has been created: %v \n", createBlogRes)

	// Read Blog
	fmt.Println("Reading the blog")
	blogID := createBlogRes.GetBlog().Id
	readBlogRes, err2 := client.ReadBlog(context.Background(), &blogpb.ReadBlogReq{Id: blogID})
	if err2 != nil {
		fmt.Printf("Error happened while reading: %v \n", err2)
	}
	fmt.Printf("Blog was read: %v \n", readBlogRes)

	// Update Blog
	fmt.Println("Updating the blog")
	updatedBlog := &blogpb.Blog{
		Id:       blogID,
		AuthorId: "Irving",
		Title:    "Go Client - Updated",
		Content:  "Content updated sent from Go Client",
	}
	updateBlogRes, err3 := client.UpdateBlog(context.Background(), &blogpb.UpdateBlogReq{Blog: updatedBlog})
	if err3 != nil {
		fmt.Printf("Error happened while updating: %v \n", err3)
	}
	fmt.Printf("Blog was updated: %v\n", updateBlogRes)

	// Delete Blog
	fmt.Println("Deleting the blog")
	deleteBlogRes, err4 := client.DeleteBlog(context.Background(), &blogpb.DeleteBlogReq{Id: blogID})
	if err4 != nil {
		fmt.Printf("Error happened while deleting: %v \n", err4)
	}
	fmt.Printf("Blog was deleted: %v \n", deleteBlogRes)

	// List Blogs
	fmt.Println("Listing the blogs")
	stream, err5 := client.ListBlogs(context.Background(), &blogpb.ListBlogsReq{})
	if err5 != nil {
		log.Fatalf("Error happened while calling ListBlog RPC: %v \n", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.ListFeatures(_) = _, %v", client, err)
		}
		fmt.Println(res.GetBlog())
	}

}
